`timescale 1ns/100ps
module generic_parser_1f(
	// INPUT PIPE
	input clk,
	input areset,
	input [255:0] datain,
	input sofin,
	input eofin,
	input validin,
	input [4:0] tagin,
	input nextheadervalid_in,
	input [4:0] nextheadertag_in,
	input [15:0] fieldin,
	input fieldin_valid,

	// OUTPUT PIPE
	output [255:0] dataout,
	output sofout,
	output eofout,
	output validout,
	output [4:0] tagout,
	
	// NEXT HEADER
	output nextheadervalid,
	output [4:0] nextheadertag,

	// FIELD EXTRACT
	output [31:0] fieldout,
	output fieldoutvalid,

    // CONFIG
    input [15:0] match_f,
    input [7:0] f1_startoffset,
    input [7:0] f1_length,
    input [7:0] headerlength // this header 
);

// Parser will extract fields from packet and 


// state machine to extract the fields and output them 
typedef enum integer { idle=0, search=1, waiteof=2 } e_state;
e_state state;
e_state state_next;

typedef enum integer { f_idle=0, f_chkagain=1, f_extract_24up = 2, 
						f_extract_15up = 3, 
						f_extract_7up= 4, 
						f_extract_3up = 5, 
						f_extract_0up = 6,
						f_waiteof=7 } e_statef;
e_statef statef;
e_statef statef_next;

reg [7:0] bytes_remain_next, bytes_remain;
reg [255:0] datain_d;


reg [31:0] fieldout_next, fieldout ; // think of it as ethertype
reg  fieldout_valid_next, fieldout_valid ; // 
reg  nextheadervalid , nextheadervalid_next;
reg [4:0] nextheadertag, nextheadertag_next ;


reg [255:0] dataout;
reg sofout, eofout, validout;
reg [4:0] tagout;
reg eofin_d;

reg [7:0] nextheadercount_next, nextheadercount;


always @(posedge clk or posedge areset) begin
	if (areset) begin
		state <= idle;
		
		nextheadervalid <= 1'b0;
		nextheadertag <= 5'd0;
		fieldout <= 16'h0000;
		fieldout_valid <= 1'b0;

		// pipeline input 
		dataout <= 256'h0000000000000000000000000000000000000000000000000000000000000000;
		sofout <= 1'b0;
		eofout <= 1'b0;
		validout <= 1'b0;
		tagout <= 5'b00000;

		eofin_d <= 1'b0;

	end else begin
		state <= state_next;
		
		nextheadervalid <= nextheadervalid_next;
		nextheadertag <= nextheadertag_next;
		
		fieldout <= fieldout_next;
		fieldout_valid <= fieldout_valid_next;

		dataout <= datain;
		sofout <= sofin;
		eofout <= eofin;
		validout <= validin;
		tagout <= tagin;

		eofin_d <= eofin;

	end
end

always@* begin
	state_next = state;
	nextheadervalid_next = nextheadervalid;
	nextheadertag_next = nextheadertag;
	

	case (state) 
		idle: begin
			nextheadervalid_next = 1'b0;
			if (nextheadervalid_in && (fieldin == match_f)) begin
				// next header and field out needs to be decoded
                 
                if (headerlength < (nextheadertag_in+1)) begin
                    nextheadervalid_next = 1'b1;
                    nextheadertag_next = nextheadertag_in  - headerlength;
                end else begin 
                    state_next = search;
                    nextheadercount_next = (headerlength - (nextheadertag_in+1));
                end

                
			end else begin 
                if (nextheadervalid_in) begin
				    // NO match - PASS
				    nextheadertag_next = nextheadertag_in;
				    nextheadervalid_next = 1'b1;
				    
                end
			end
		end

        search : begin
            // SEARCH FOR END OF HEADER
            if (nextheadercount >= 6'd32 ) begin
                nextheadercount_next = nextheadercount - 6'd32;
            end else begin
                state_next = waiteof;
                nextheadertag_next = 31 - nextheadercount ; // start at top 
                nextheadervalid_next = 1'b1;
            end
            if (eofin) begin
                state_next = idle;
            end
        end

        waiteof : begin
            if (eofin) begin
                state_next = idle;
            end
        end
		default: begin
			state_next = idle;
		end

	endcase
end


// Field extractors 
always@(posedge clk or posedge areset) begin
	if (areset) begin
		datain_d <= {256{1'b0}};
		statef <= idle;
		fieldout <= 32'h00000000;
		fieldout_valid <= 1'b0;
		bytes_remain <= 8'h00;
	end else begin
		datain_d <= datain;
		statef <= statef_next;
		fieldout <= fieldout_next;
		fieldout_valid <= fieldout_valid_next;
		bytes_remain <= bytes_remain_next;
	end
end

// state machine looks at 1 clock delayed data for extrcation
// and extracts the field based on splitting possibility space
always@* begin
	statef_next = statef;
	
	fieldout_valid_next = fieldout_valid;	
	fieldout_next = fieldout;
	bytes_remain_next = bytes_remain;

	case (statef) 
		f_idle: begin
			fieldout_valid_next = 1'b0;
		    if (nextheadervalid_in && (fieldin == match_f)) begin
		         // next header and field out needs to be decoded


                         // NOT IN THIS CLOCK  
                         if (f1_startoffset > (nextheadertag_in)) begin
                             bytes_remain_next = f1_startoffset - nextheadertag_in  ;
                             statef_next = f_chkagain;
                         // 24 or higher location
                         end else if ((nextheadertag_in - f1_startoffset) > 24) begin
                             statef_next = f_extract_24up;
                             bytes_remain_next = -f1_startoffset + nextheadertag_in ;
                         // 16 and up
                         end  else if ((nextheadertag_in - f1_startoffset) > 15) begin
                             statef_next = f_extract_15up;
                             bytes_remain_next = -f1_startoffset + nextheadertag_in  ;
                         // 7 up
                         end else if ((nextheadertag_in - f1_startoffset) > 7) begin
                             statef_next = f_extract_7up;
                             bytes_remain_next = -f1_startoffset + nextheadertag_in  ;
                         // 3
                         end else if ((nextheadertag_in - f1_startoffset) > 2) begin
                             statef_next = f_extract_3up;
                             bytes_remain_next = -f1_startoffset + nextheadertag_in  ;
                         
                         // 0,1,2
                         end else begin
                             statef_next = f_extract_0up;
                             bytes_remain_next = f1_startoffset - nextheadertag_in -1 ;
                         end
		    end 
		end

        f_chkagain : begin
			// this state 
			// NOT IN THIS CLOCK  
            if (bytes_remain > (nextheadertag_in+1)) begin
                bytes_remain_next = bytes_remain - nextheadertag_in -1 ;
                statef_next = f_chkagain;
            // 24 or higher location
            end else if ((nextheadertag_in+1 - bytes_remain) > 23) begin
                statef_next = f_extract_24up;
                bytes_remain_next = -bytes_remain + nextheadertag_in +1 ;
            // 16 and up
            end  else if ((nextheadertag_in+1 - bytes_remain) > 15) begin
                statef_next = f_extract_15up;
                bytes_remain_next = -bytes_remain + nextheadertag_in +1 ;
            // 7 up
            end else if ((nextheadertag_in+1 - bytes_remain) > 7) begin
                statef_next = f_extract_7up;
                bytes_remain_next = -bytes_remain + nextheadertag_in  +1;
            // 3
            end else if ((nextheadertag_in+1 - bytes_remain) > 2) begin
                statef_next = f_extract_3up;
                bytes_remain_next = -bytes_remain + nextheadertag_in +1 ;
            // 0,1,2
            end else begin
                statef_next = f_extract_0up;
                bytes_remain_next = -bytes_remain + nextheadertag_in +1 ;
            end
        end

        f_extract_24up : begin
			case(bytes_remain)
				31: begin
					statef_next = f_waiteof;
					fieldout_next[31:0] = datain_d[255: 224];
					fieldout_valid_next = 1'b1;
				end

				30: begin
					statef_next = f_waiteof;
					fieldout_next[31:0] = datain_d[247: 216];
					fieldout_valid_next = 1'b1;
				end

				29: begin
					statef_next = f_waiteof;
					fieldout_next[31:0] = datain_d[239: 208];
					fieldout_valid_next = 1'b1;
				end

				28: begin
					statef_next = f_waiteof;
					fieldout_next[31:0] = datain_d[231: 200];
					fieldout_valid_next = 1'b1;
				end

				27: begin
					statef_next = f_waiteof;
					fieldout_next[31:0] = datain_d[223: 192];
					fieldout_valid_next = 1'b1;
				end

				26: begin
					statef_next = f_waiteof;
					fieldout_next[31:0] = datain_d[215: 184];
					fieldout_valid_next = 1'b1;
				end

				25: begin
					statef_next = f_waiteof;
					fieldout_next[31:0] = datain_d[207: 176];
					fieldout_valid_next = 1'b1;
				end

				24: begin
					statef_next = f_waiteof;
					fieldout_next[31:0] = datain_d[199: 168];
					fieldout_valid_next = 1'b1;
				end

			endcase
        end

        f_extract_15up: begin
			case(bytes_remain)
				16 : begin
					statef_next = f_waiteof;
					fieldout_next[31:0] = datain_d[136 : 104];
					fieldout_valid_next = 1'b1;
				end

				17 : begin// we have to extrcat 4 bytes from here - seems possible to wrap up here and done 
					statef_next = f_waiteof;
					fieldout_next[31:0] = datain_d[144 : 112];
					fieldout_valid_next = 1'b1;
				end

				18 : begin
				end 

				19 : begin
				end 

				20 : begin
				end 

				21 : begin
				end 

				22 : begin
				end 

				23 : begin

				end

			endcase
        end

        f_extract_7up: begin
			case(bytes_remain)
				8 : begin
				end

				

				9 : begin
				end 

				10 : begin
				end 

				11 : begin
				end 

				12 : begin
				end 

				13 : begin
				end 

				14 : begin
					// we have to extrcat 4 bytes from here - seems possible to wrap up here and done 
					statef_next = f_waiteof;
					fieldout_next[31:0] = datain_d[119 : 88];
					fieldout_valid_next = 1'b1;
				end

				15 : begin
					// we have to extrcat 4 bytes from here - seems possible to wrap up here and done 
					statef_next = f_waiteof;
					fieldout_next[31:0] = datain_d[127 : 96];
					fieldout_valid_next = 1'b1;
				end
			endcase
        end

        f_extract_3up : begin

        end

        f_extract_0up: begin
        end

		f_waiteof: begin
			if (eofin_d) begin
				statef_next = f_idle;
			end
		end

		default: begin
			statef_next = f_idle;
		end

	endcase
end

endmodule
