`timescale 1ns/100ps
module l2_parser(
	// INPUT PIPE
	input clk,
	input areset,
	input [255:0] datain,
	input sofin,
	input eofin,
	input validin,
	input [4:0] tagin,

	// OUTPUT PIPE
	output [255:0] dataout,
	output sofout,
	output eofout,
	output validout,
	output [4:0] tagout,
	
	// NEXT HEADER
	output nextheadervalid,
	output [4:0] nextheadertag,

	// FIELD EXTRACT
	output [47:0] macda,
	output macdavalid,
	output [47:0] macsa,
	output macsavalid,
	output [15:0] macether,
	output macether_valid
);


// Parser will extract fields from packet and 
// state machine to extract the fields and output them 
typedef enum integer { idle=0, dest=1, source=2, ether=3 } e_state1;
e_state1 state;
e_state1 state_next;
reg [47:0] macda_next ;
reg  macda_valid_next ;
reg [47:0] macsa_next ;
reg  macsa_valid_next ;
reg [15:0] macether_next ;
reg  macether_valid_next ;
reg  nextheadervalid_next ;
reg [4:0] nextheadertag_next ;
reg [47:0] macda ;
reg  macda_valid ;
reg [47:0] macsa ;
reg  macsa_valid ;
reg [15:0] macether ;
reg  macether_valid ;
reg  nextheadervalid ;
reg [4:0] nextheadertag ;
reg [255:0] dataout;
reg sofout, eofout, validout;
reg [4:0] tagout;


always @(posedge clk or posedge areset) begin
	if (areset) begin
		state <= idle;
		macda <= 48'h000000000000;
		macda_valid <= 1'b0;
		macsa <= 48'h000000000000;
		macsa_valid <= 1'b0;
		macether <= 16'h0000;
		macether_valid <= 1'b0;
		nextheadervalid <= 1'b0;
		nextheadertag <= 5'd0;

		// pipeline input 
		dataout <= 256'h0000000000000000000000000000000000000000000000000000000000000000;
		sofout <= 1'b0;
		eofout <= 1'b0;
		validout <= 1'b0;
		tagout <= 5'b00000;

	end else begin
		state <= state_next;
		macda <= macda_next;
		macda_valid <= macda_valid_next;
		macsa <= macsa_next;
		macsa_valid <= macsa_valid_next;
		macether <= macether_next;
		macether_valid <= macether_valid_next;
		nextheadervalid <= nextheadervalid_next;
		nextheadertag <= nextheadertag_next;
		dataout <= datain;
		sofout <= sofin;
		eofout <= eofin;
		validout <= validin;
		tagout <= tagin;
	end
end

always@* begin
	state_next = state;
	macda_next = macda;
	macda_valid_next = macda_valid;
	macsa_next = macsa;
	macsa_valid_next = macsa_valid;
	macether_next = macether;
	macether_valid_next = macether_valid;
	nextheadervalid_next = nextheadervalid;
	nextheadertag_next = nextheadertag;

	case (state) 
		idle: begin
			macda_next = 48'h000000000000;
			nextheadervalid_next = 1'b0;
			if (sofin) begin
				state_next = idle;
				macda_next = datain[255:208];
				macda_valid_next = 1'b1;
				macsa_next = datain[207:160];
				macsa_valid_next = 1'b1;
				macether_next = datain[159:144];
				macether_valid_next = 1'b1;
				nextheadervalid_next = 1'b1;
				nextheadertag_next = 5'd19;
			end 
			if (eofin) begin
				macda_valid_next = 1'b0;
				macsa_valid_next = 1'b0;
				macether_valid_next = 1'b0;
				nextheadervalid_next = 1'b0;
			end
		end

		default: begin
			state_next = idle;

		end

	endcase
end

endmodule
