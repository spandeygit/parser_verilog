`timescale 1ns/100ps

module tb() ;


reg 		clk, areset;
reg [255:0] 	datain;
reg 		sofin;
reg 		eofin;
reg 		validin;
reg [4:0] 	tagin;

// mac stage 
wire [255:0] mac_dataout;
wire mac_eofout, mac_sofout, mac_validout;
wire [4:0] mac_tagout;
wire [15:0] mac_ethertype;
wire mac_ethertype_valid;
wire [4:0] mac_nextheadertag;
wire mac_nextheadervalid;

wire [255:0] vlan_dataout;
wire vlan_sofout, vlan_eofout, vlan_validout;
wire [4:0] vlan_tagout;
wire [4:0] vlan_nextheadertag;
wire vlan_nextheadervalid;
wire [15:0] vlan_ethertype;
wire  vlan_ethertype_valid;

wire [255:0] gen1_dataout;
wire gen1_eofout;
wire [31:0] gen1_fieldout;
wire gen1_fieldoutvalid;
wire [4:0] gen1_nextheadertag;
wire gen1_nextheadervalid;
wire gen1_sofout;
wire [4:0] gen1_tagout;
wire gen1_validout;



l2_parser l2_inst(
	.clk(clk),
	.areset(areset),
	.datain(datain),
	.sofin(sofin),
	.eofin(eofin),
	.validin(validin),
	.tagin(tagin),

	// OUTPUT PIPE
	.dataout(mac_dataout),
	.sofout(mac_sofout),
	.eofout(mac_eofout),
	.validout(mac_validout),
	.tagout(mac_tagout),
	
	// NEXT HEADER
	.nextheadervalid(mac_nextheadervalid),
	.nextheadertag(mac_nextheadertag),

	// FIELD EXTRACT
	.macda(),
	.macdavalid(),
	.macsa(),
	.macsavalid(),
	.macether(mac_ethertype),
	.macether_valid(mac_ethertype_valid)
);


 vlan_parser vlan_parser_int(
	// INPUT PIPE
	.clk(clk),
	.areset(areset),
	.datain(mac_dataout),
	.sofin(mac_sofout),
	.eofin(mac_eofout),
	.validin(mac_validout),
	.tagin(mac_tagout),

	.nextheadervalid_in(mac_nextheadervalid),
	.nextheadertag_in(mac_nextheadertag),
	.mac_ethertype(mac_ethertype),
	.mac_ethertype_valid(mac_ethertype_valid),


	.dataout(vlan_dataout),
	.sofout(vlan_sofout),
	.eofout(vlan_eofout),
	.validout(vlan_validout),
	.tagout(vlan_tagout),
	.nextheadervalid(vlan_nextheadervalid),
	.nextheadertag(vlan_nextheadertag),
	
	
	
	.vlan2(),
	.vlan2valid(),
	.vlan1(),
	.vlan1valid(),
	.ethertype(vlan_ethertype),
	.ethertype_valid(vlan_ethertype_valid)
);

 generic_parser_1f generic_parser_if_inst(
	// INPUT PIPE
	 .clk(clk),
	 .areset(areset),
	 .datain(mac_dataout),
	 .sofin(mac_sofout),
	 .eofin(mac_eofout),
	 .validin(mac_validout),
	 .tagin(mac_tagout),
	 .nextheadervalid_in(mac_nextheadervalid),
	 .nextheadertag_in(mac_nextheadertag),
	 .fieldin(mac_ethertype),
	 .fieldin_valid(mac_ethertype_valid),

	// OUTPUT PIPE
	 .dataout(gen1_dataout),
	 .sofout(gen1_sofout),
	 .eofout(gen1_eofout),
	 .validout(gen1_validout),
	 .tagout(gen1_tagout),
	
	// NEXT HEADER
	 .nextheadervalid(gen1_nextheadervalid),
	 .nextheadertag(gen1_nextheadertag),

	// FIELD EXTRACT
	 .fieldout(gen1_fieldout),
	 .fieldoutvalid(gen1_fieldoutvalid),

    // CONFIG
     .match_f(16'h8100),
     .f1_startoffset(8'h3),
     .f1_length(8'h4),
     .headerlength(8'h4) // this header 
);

initial
 begin
    $dumpfile("test.vcd");
    $dumpvars(0,tb);
 end

initial begin
	clk <= 0;
	areset <= 1;
	datain = 256'h0000000000000000000000000000000000000000000000000000000000000000; 
	sofin = 1'b0;
	eofin = 1'b0;
	validin = 1'b0;
	tagin <= 5'b00000;
	#202 areset <= 0;
	#10 datain = 256'hddddddddddddaaaaaaaaaaaa8100010281000506080009101112131415161718; 
	sofin = 1'b1;
	eofin = 1'b0;
	validin = 1'b1;
	tagin <= 5'b11111;
	#10 datain = 256'h1900000000000000000000000000000000000000000000000000000000000000; 
	sofin = 1'b0;
	eofin = 1'b1;
	validin = 1'b1;
	tagin <= 5'b00000;
	#10 datain = 256'h0000000000000000000000000000000000000000000000000000000000000000; 
	sofin = 1'b0;
	eofin = 1'b0;
	validin = 1'b0;
	tagin <= 5'b00000;

	#1000 $finish();
end

always @(clk) #5 clk <= !clk;


endmodule
