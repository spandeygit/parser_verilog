`timescale 1ns/100ps
module vlan_parser(
	// INPUT PIPE
	input clk,
	input areset,
	input [255:0] datain,
	input sofin,
	input eofin,
	input validin,
	input [4:0] tagin,
	input nextheadervalid_in,
	input [4:0] nextheadertag_in,
	input [15:0] mac_ethertype,
	input mac_ethertype_valid,

	// OUTPUT PIPE
	output [255:0] dataout,
	output sofout,
	output eofout,
	output validout,
	output [4:0] tagout,
	
	// NEXT HEADER
	output nextheadervalid,
	output [4:0] nextheadertag,

	// FIELD EXTRACT
	output [15:0] vlan1,
	output vlan1valid,
	output [15:0] vlan2,
	output vlan2valid,
	output [15:0] ethertype,
	output ethertype_valid
);

// Parser will extract fields from packet and 


// state machine to extract the fields and output them 
typedef enum integer { idle=0, dest=1, source=2, ether=3 } e_state;
e_state state;
e_state state_next;
reg [15:0] vlan1_next ;
reg  vlan1_valid_next ;
reg [15:0] vlan2_next ;
reg  vlan2_valid_next ;


reg [15:0] ethertype;
reg [15:0] ethertype_next;
reg ethertype_valid, ethertype_valid_next;


reg [15:0] vlan1 ;
reg  vlan1_valid ;
reg [15:0] vlan2 ;
reg  vlan2_valid ;
reg  nextheadervalid , nextheadervalid_next;
reg [4:0] nextheadertag, nextheadertag_next ;


reg [255:0] dataout;
reg sofout, eofout, validout;
reg [4:0] tagout;
reg eofin_d;



always @(posedge clk or posedge areset) begin
	if (areset) begin
		state <= idle;
		
		nextheadervalid <= 1'b0;
		nextheadertag <= 5'd0;
		vlan1 <= 16'h0000;
		vlan1_valid <= 1'b0;
		vlan2 <= 16'h0000;
		vlan2_valid <= 1'b0;
		ethertype <= 16'h0000;
		ethertype_valid <= 1'b0;

		// pipeline input 
		dataout <= 256'h0000000000000000000000000000000000000000000000000000000000000000;
		sofout <= 1'b0;
		eofout <= 1'b0;
		validout <= 1'b0;
		tagout <= 5'b00000;

		eofin_d <= 1'b0;

	end else begin
		state <= state_next;
		
		nextheadervalid <= nextheadervalid_next;
		nextheadertag <= nextheadertag_next;
		vlan1 <= vlan1_next;
		vlan1_valid <= vlan1_valid_next;
		vlan2 <= vlan2_next;
		vlan2_valid <= vlan2_valid_next;
		ethertype <= ethertype_next;
		ethertype_valid <= ethertype_valid_next;

		dataout <= datain;
		sofout <= sofin;
		eofout <= eofin;
		validout <= validin;
		tagout <= tagin;

		eofin_d <= eofin;

	end
end

always@* begin
	state_next = state;
	nextheadervalid_next = nextheadervalid;
	nextheadertag_next = nextheadertag;
	ethertype_valid_next = ethertype_valid;
	vlan1_valid_next = vlan1_valid;
	vlan2_valid_next = vlan2_valid;
	ethertype_next = ethertype;
	vlan1_next = vlan1;
	vlan2_next = vlan2;



	case (state) 
		idle: begin
			nextheadervalid_next = 1'b0;
			if (nextheadervalid_in && (mac_ethertype == 16'h8100)) begin
				// if vlan tag is found, it must be at location right after MAC 
				// so byte 17,16,15,14  and 13,12,11,10 in same clock as SOF 
				// third VLAN can be in 9,8,7,6
				vlan1_next = datain[143 : 128];
				vlan1_valid_next = 1'b1;
				ethertype_valid_next = 1'b1;
				nextheadervalid_next = 1'b1;
				if (datain[127: 112] == 16'h8100) begin
					vlan2_next = datain[111 : 96];
					vlan2_valid_next = 1'b1;
					nextheadertag_next = 5'd9;
					ethertype_next = datain[95:80];
				end else begin
					nextheadertag_next = 5'd13;
					ethertype_next = datain[127:112];
				end
				nextheadervalid_next = 1'b1;
			end else if (nextheadervalid_in) begin
				// NO VLANS 
				nextheadertag_next = 5'd17;
				nextheadervalid_next = 1'b1;
				ethertype_next = datain[143:128];
				vlan1_next = 16'h0000;
				vlan2_next = 16'h0000;
				vlan1_valid_next = 1'b0;
				vlan2_valid_next = 1'b0;
			end

			if (eofin_d) begin
				vlan1_valid_next = 1'b0;
				vlan2_valid_next = 1'b0;
				nextheadervalid_next = 1'b0;
			end
		end

		default: begin
			state_next = idle;
		end

	endcase
end

endmodule
